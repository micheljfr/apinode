const express = require('express');
var bodyParser = require('body-parser')
const app = express();
const router = express.Router();

var cors = require('cors');

// use it before all route definitions
app.use(cors({origin: '*'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())

//Rotas
const index = require('./routes/index');
const personRoute = require('./routes/personRoute');
app.use('/', index);
app.use('/persons', personRoute);
module.exports = app;