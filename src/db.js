const mongoose = require('mongoose');
//const credenciaisMlab = require("./credenciaisMlab");
mongoose.connect(`mongodb://${process.env.USER_MLAB}:${process.env.PSW_MLAB}@ds163410.mlab.com:63410/apinode`);

const customerSchema = new mongoose.Schema({
    nome: String,
    sobrenome: String,
    email: String
}, { collection: 'customers' }
);
 
module.exports = { Mongoose: mongoose, CustomerSchema: customerSchema }